import express from 'express';
import json from 'body-parser';
export const router = express.Router();

router.get('/', (req,res)=>{

    const params = {
       numeroDocente : req.query.numeroDocente,
       nombre : req.query.nombre,
       domicilio: req.query.domicilio,
       nivel: req.query.nivel,
       PHB: req.query.PHB,
       horasImpartidas: req.query.horasImpartidas,
       hijos: req.query.hijos
    }
    res.render('index',params);
})

router.post('/', (req,res)=>{
    const params = {
        numeroDocente : req.body.numeroDocente,
        nombre : req.body.nombre,
        domicilio: req.body.domicilio,
        nivel: req.body.nivel,
        PHB: req.body.PHB,
        horasImpartidas: req.body.horasImpartidas,
        hijos: req.body.hijos
     }
     res.render('index',params);
 })

 export default {router}