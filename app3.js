import http from 'http';
import path from 'path'; 
const puerto = 80;
import express from 'express'
import json from 'body-parser'
import { fileURLToPath } from 'url';
import misRutas from './router/index.js'

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


const app = express();
//Asignaciones
app.set("view engine","ejs");

app.use(json.urlencoded({extends:true}));

//asignar al objeto informacion
app.set(express.static(__dirname + '/public'));

app.use(misRutas.router)

app.listen(puerto,()=>{

    console.log("escuchando servidor")

})